{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE DeriveGeneric #-}
module Lib
    ( someFunc
    , run
    ) where

import           Control.Monad
import           Data.Aeson
import qualified Data.Aeson as Aeson
import qualified Data.ByteString      as BS
import qualified Data.ByteString.Lazy as LBS
import           Data.Text            as T
import qualified Data.Text.Encoding   as TE
import           Network.HTTP.Client
import           System.Cron.Schedule
import Control.Concurrent
import GHC.Generics

data Task a = Task
  { sendTo   :: T.Text
  , content  :: a
  , cronTime :: T.Text
  } deriving (Show,Eq)

data Message = Message
 { hello :: String
 } deriving (Show, Eq, Generic)

instance ToJSON Message

handle :: ToJSON a => Task a -> IO (Response LBS.ByteString)
handle Task {..} = do
  manager <- newManager defaultManagerSettings
  initReq <- parseUrl (T.unpack sendTo)
  let req = initReq
          { method = "POST"
          , requestBody = RequestBodyBS (LBS.toStrict $ Aeson.encode content)
          }
  httpLbs req manager

tasks_ :: [Task Message]
tasks_ =
  [ Task "http://example.com" (Message "world") "* * * * *"
  ]

run :: IO ()
run = do
  ts <- newEmptyMVar
  putMVar ts tasks_
  tasks <- readMVar ts
  do tids <-
       execSchedule $
       forM_ tasks $ \task ->
           addJob (handle task >>= print) (T.unpack $ cronTime task)
     print tids

someFunc :: IO ()
someFunc = putStrLn "someFunc"
